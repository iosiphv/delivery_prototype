package com.ua.rs;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentFactory;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ua.rs.databinding.ActivityMainBinding;
import com.ua.rs.vm.BottomNavViewModel;
import com.ua.rs.vm.CartViewModel;

import static androidx.navigation.Navigation.findNavController;
//import com.crashlytics.android.Crashlytics;

public class MainActivity extends AppCompatActivity {

	private ActivityMainBinding bind;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//		Crashlytics.getInstance();
//		getSupportFragmentManager().setFragmentFactory(new MyFactory());
		super.onCreate(savedInstanceState);

		bind = DataBindingUtil.setContentView(this, R.layout.activity_main);
		NavController navController = findNavController(this, R.id.nav_container);

		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		if (user == null) FirebaseAuth.getInstance().signInAnonymously();

		BottomNavigationView bottomNV = bind.bottomNavigationView;
		NavigationUI.setupWithNavController(bottomNV, navController);

		ViewModelStoreOwner storeOwner =
				navController.getViewModelStoreOwner(R.id.nav_graph);
		ViewModelProvider provider = new ViewModelProvider(storeOwner,
				getDefaultViewModelProviderFactory());

		BottomNavViewModel bottomNavViewModel = provider.get(BottomNavViewModel.class);

		bottomNavViewModel
				.getCount()
				.observe(this, number -> {
					Log.d("Observe", "" + number);
					BadgeDrawable badgeDrawable =
							bottomNV.getOrCreateBadge(R.id.fragment_basket);
					if (number == 0)
						badgeDrawable.setVisible(false, true);
					else {
						badgeDrawable.setVisible(true, true);
						badgeDrawable.setNumber(number);
					}
				});

		CartViewModel cartViewModel = provider.get(CartViewModel.class);
		cartViewModel
				.getData()
				.observe(this, items -> bottomNavViewModel.setCount(items.size()));
		cartViewModel.loadCart();
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		NavController navController = findNavController(this, R.id.nav_container);
		return NavigationUI.onNavDestinationSelected(item, navController) || super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onSupportNavigateUp() {
		return true;
	}

//	private class MyFactory extends FragmentFactory {
//		@NonNull
//		@Override
//		public Fragment instantiate(@NonNull ClassLoader classLoader, @NonNull String className) {
//			Fragment fragment;
//			switch (className) {
//				case "TradePointFragment":
//					fragment = TradePointFragment.newInstance();
//					break;
//				default:
//					fragment = super.instantiate(classLoader, className);
//					break;
//			}
//
//			return fragment;
//		}
//	}
}
