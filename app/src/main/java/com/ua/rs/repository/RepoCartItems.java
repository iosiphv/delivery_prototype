package com.ua.rs.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.ua.rs.pojo.OrderItem;

public class RepoCartItems {
	private final String TAG          = "CloudCartRepo";
	private final String CART_DOC_REF = "CART";

	private final FirebaseFirestore db;

	public RepoCartItems() {
		db = FireStoreHelper.getFirestoreInstance();
	}

	public void addToCart(@NonNull OrderItem item,
	                      @NonNull OnCompleteListener<Void> onCompleteListener
	) {
		db
				.collection("Users")
				.document(getCurrentUserUID())
				.collection(CART_DOC_REF)
				.document()
				.set(item, SetOptions.merge())
				.addOnFailureListener(e -> Log.e(TAG, "addToCart: " + e.getLocalizedMessage()))
				.addOnCompleteListener(onCompleteListener);
	}

	public void loadCart(@NonNull OnCompleteListener<QuerySnapshot> onCompleteListener) {
		db
				.collection("Users")
				.document(getCurrentUserUID())
				.collection(CART_DOC_REF)
				.get()
				.addOnCompleteListener(onCompleteListener)
				.addOnFailureListener(e -> Log.e(TAG, "loadCart: ", e))
		;
	}

	public void clearCart(@NonNull OnCompleteListener<QuerySnapshot> onCompleteListener) {
		db
				.collection("Users")
				.document(getCurrentUserUID())
				.collection(CART_DOC_REF)
				.get()
				.addOnCompleteListener(onCompleteListener)
				.addOnFailureListener(e -> Log.e(TAG, "clearCart: ", e))
		;
	}

	private String getCurrentUserUID() {
		if (FirebaseAuth.getInstance().getCurrentUser() == null) return "";
		return FirebaseAuth.getInstance().getCurrentUser().getUid();
	}
}