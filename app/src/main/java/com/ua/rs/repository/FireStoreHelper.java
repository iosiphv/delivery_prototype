package com.ua.rs.repository;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;

public final class FireStoreHelper {
	private static FirebaseFirestore instance;

	public static FirebaseFirestore getFirestoreInstance() {
		if (instance == null) {
			if (FirebaseAuth.getInstance().getCurrentUser() == null)
				FirebaseAuth.getInstance().signInAnonymously();

			FirebaseFirestoreSettings build = new FirebaseFirestoreSettings.Builder()
					.setCacheSizeBytes(1024 * 1024)//1mb
					.setPersistenceEnabled(true)
					.build();
			FirebaseFirestore db = FirebaseFirestore.getInstance();
			db.setFirestoreSettings(build);
			instance = db;
		}
		return instance;
	}
}
