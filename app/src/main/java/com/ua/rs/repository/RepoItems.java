package com.ua.rs.repository;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class RepoItems {
	private static final String TAG = "RepoItems";

	public void getDataFromNet(@NonNull OnSuccessListener<QuerySnapshot> onSuccessListener) {
		FireStoreHelper
				.getFirestoreInstance()
				.collection("Items")
				.get()
				.addOnSuccessListener(onSuccessListener)
				.addOnFailureListener(e -> Log.e(TAG, "getDataFromNet: ", e))
		;
	}

	public void getItemByDocRef(@NonNull DocumentReference refs,
	                         @NonNull OnSuccessListener<DocumentSnapshot> onSuccessListener
	) {
		FireStoreHelper
				.getFirestoreInstance();
		refs.get()
		    .addOnSuccessListener(onSuccessListener)
		    .addOnFailureListener(e -> Log.e(TAG, "getItemByRef: ", e))
		;
	}
}
