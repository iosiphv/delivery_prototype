package com.ua.rs.vm;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.ua.rs.pojo.Item;
import com.ua.rs.pojo.OrderItem;
import com.ua.rs.pojo.OrderItemWrapper;
import com.ua.rs.repository.RepoCartItems;

import java.util.ArrayList;

public class CartViewModel extends ViewModel {
	private final String TAG = "CloudCart";

	private MutableLiveData<ArrayList<OrderItem>> liveData = new MutableLiveData<>();

	private ArrayList<OrderItem>        items        = new ArrayList<>(5);
	private ArrayList<OrderItemWrapper> itemsWrapper = new ArrayList<>(5);

	private RepoCartItems repoCartItems = new RepoCartItems();

	public void loadCart() {
		repoCartItems.loadCart(task -> {
			items = new ArrayList<>(5);
			QuerySnapshot result = task.getResult();
			items.addAll(result.toObjects(OrderItem.class));
			liveData.postValue(items);
		});
	}

	public void addToCart(@NonNull Item item) {
		OrderItem orderItem = new OrderItem(item.getPath(), item.getPrice(), 1);
		repoCartItems.addToCart(orderItem, task -> {
			items.add(orderItem);
			liveData.postValue(items);
		});
	}

	public void clearCart() {
		repoCartItems.clearCart(task -> {
					for (QueryDocumentSnapshot snap : task.getResult()) snap.getReference().delete();
					items.clear();
					liveData.postValue(items);
				}
		);
	}

	public LiveData<ArrayList<OrderItem>> getData() {
		if (liveData == null) liveData = new MutableLiveData<>();
		return liveData;
	}
}
