package com.ua.rs.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ua.rs.pojo.Item;
import com.ua.rs.repository.RepoItems;

import java.util.ArrayList;

public class TradePointViewModel extends ViewModel {
	private final String TAG = "TradePointVM";

	private MutableLiveData<ArrayList<Item>> data = new MutableLiveData<>();

	private RepoItems repo;

	public TradePointViewModel() {
		repo = new RepoItems();
	}

	public void getDataFromNet() {
		repo.getDataFromNet(qDocSnapshots -> {
			ArrayList<Item> tempItems = new ArrayList<>(10);
			for (QueryDocumentSnapshot doc : qDocSnapshots) {
				Item item = doc.toObject(Item.class);
				item.setPath(doc.getReference());
				tempItems.add(item);
			}
			if (tempItems.size() > 0) data.postValue(tempItems);
		});
	}

	public LiveData<ArrayList<Item>> getData() {
		if (data == null) data = new MutableLiveData<>();
		return data;
	}
}
