package com.ua.rs.vm;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BottomNavViewModel extends ViewModel {
	private MutableLiveData<Integer> cartItemCount = new MutableLiveData<>();

	public void setCount(int count) {
		cartItemCount.postValue(count);
	}

	public LiveData<Integer> getCount() {
		return cartItemCount;
	}
}
