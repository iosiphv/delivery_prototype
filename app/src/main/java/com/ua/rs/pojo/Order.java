package com.ua.rs.pojo;

import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentId;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.ArrayList;

public class Order {
	@DocumentId
	private String order_num;

	@ServerTimestamp
	private Timestamp order_create_timestamp;

	private Timestamp delivery_timestamp;

	private DocumentReference delivery_point;
	private DocumentReference payment_status;
	private DocumentReference payment_type;

	private ArrayList<OrderItem> items;

	public String getOrder_num() {
		return order_num;
	}

	public void setOrder_num(String order_num) {
		this.order_num = order_num;
	}

	public Timestamp getOrder_create_timestamp() {
		return order_create_timestamp;
	}

	public void setOrder_create_timestamp(Timestamp order_create_timestamp) {
		this.order_create_timestamp = order_create_timestamp;
	}

	public Timestamp getDelivery_timestamp() {
		return delivery_timestamp;
	}

	public void setDelivery_timestamp(Timestamp delivery_timestamp) {
		this.delivery_timestamp = delivery_timestamp;
	}

	public DocumentReference getDelivery_point() {
		return delivery_point;
	}

	public void setDelivery_point(DocumentReference delivery_point) {
		this.delivery_point = delivery_point;
	}

	public DocumentReference getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(DocumentReference payment_status) {
		this.payment_status = payment_status;
	}

	public DocumentReference getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(DocumentReference payment_type) {
		this.payment_type = payment_type;
	}

	public ArrayList<OrderItem> getItems() {
		return items;
	}

	public void setItems(ArrayList<OrderItem> items) {
		this.items = items;
	}
}
