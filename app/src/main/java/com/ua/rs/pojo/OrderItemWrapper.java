package com.ua.rs.pojo;

import androidx.annotation.NonNull;

import java.util.Objects;

public class OrderItemWrapper {
	private Item      itemInfo;
	private OrderItem orderItem;

	public OrderItemWrapper(@NonNull Item itemInfo, @NonNull OrderItem orderItem) {
		this.itemInfo = itemInfo;
		this.orderItem = orderItem;
	}

	public Item getItemInfo() {
		return itemInfo;
	}

	public void setItemInfo(@NonNull Item itemInfo) {
		this.itemInfo = itemInfo;
	}

	public OrderItem getOrderItem() {
		return orderItem;
	}

	public void setOrderItem(@NonNull OrderItem orderItem) {
		this.orderItem = orderItem;
	}

	@Override
	public int hashCode() {
		return Objects.hash(itemInfo.hashCode(), orderItem.hashCode());
	}

	@NonNull
	@Override
	public String toString() {
		return itemInfo.toString() + " " + orderItem.toString();
	}
}
