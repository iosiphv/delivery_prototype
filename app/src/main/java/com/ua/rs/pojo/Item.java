package com.ua.rs.pojo;

import com.google.firebase.firestore.DocumentId;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;

import java.util.Objects;

public class Item {
	@DocumentId
	private String id;

	private String name;
	private String about;
	private float  weight;
	private float  price;
	private String currency;
	private String imgName;
	private String group;

	@Exclude
	private DocumentReference path;

	public Item() {
	}

	public Item(String name,
	            String about,
	            float weight,
	            float price,
	            String currency,
	            String imgName,
	            String group
	) {
		this.name = name;
		this.about = about;
		this.weight = weight;
		this.price = price;
		this.currency = currency;
		this.imgName = imgName;
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public String getAbout() {
		return about;
	}

	public float getWeight() {
		return weight;
	}

	public float getPrice() {
		return price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Item item = (Item) o;
		return Float.compare(item.weight, weight) == 0 &&
				Float.compare(item.price, price) == 0 &&
				name.equals(item.name) &&
				about.equals(item.about) &&
				currency.equals(item.currency);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, about, weight, price, currency);
	}

	public String getImgName() {
		return imgName;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public void setPath(DocumentReference docReference) {
		this.path = docReference;
	}


	public DocumentReference getPath() {
		return path;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
