package com.ua.rs.pojo;

import com.google.firebase.firestore.DocumentId;
import com.google.firebase.firestore.DocumentReference;

import java.util.Objects;

public class OrderItem {
	@DocumentId
	private String id;

	private DocumentReference itemInfoDocPath;

	private float orderedPrice;
	private int   orderedQuantity;
	private float orderedSum;

	public OrderItem() {
	}

	public OrderItem(DocumentReference itemInfoDocPath,
	                 float orderedPrice,
	                 int orderedQuantity
	) {
		this.itemInfoDocPath = itemInfoDocPath;
		this.orderedPrice = orderedPrice;
		this.orderedQuantity = orderedQuantity;
		this.orderedSum = orderedQuantity * orderedPrice;
	}

	public void addOne() {
		setOrderedQuantity(orderedQuantity++);
	}

	public void removeOne() {
		setOrderedQuantity(orderedQuantity--);
	}

	public int getOrderedQuantity() {
		return orderedQuantity;
	}

	public float getOrderedSum() {
		return orderedSum;
	}

	public void setOrderedQuantity(int orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
		orderedSum = orderedPrice * orderedQuantity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof OrderItem)) return false;
		OrderItem orderItem = (OrderItem) o;
		return Float.compare(orderItem.orderedQuantity, orderedQuantity) == 0 &&
				Float.compare(orderItem.orderedSum, orderedSum) == 0;
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), orderedQuantity);
	}

	public DocumentReference getItemInfoDocPath() {
		return itemInfoDocPath;
	}

	public float getOrderedPrice() {
		return orderedPrice;
	}

	public void setOrderedPrice(float orderedPrice) {
		this.orderedPrice = orderedPrice;
	}

	public void setOrderedSum(float orderedSum) {
		this.orderedSum = orderedSum;
	}

	public void setItemInfoDocPath(DocumentReference itemInfoDocPath) {
		this.itemInfoDocPath = itemInfoDocPath;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
