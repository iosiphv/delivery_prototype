package com.ua.rs.decorators;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GridItemDecoration extends RecyclerView.ItemDecoration {
	private final int space;

	public GridItemDecoration(Context context, @DimenRes int offsetInDP) {
		space = context.getResources().getDimensionPixelSize(offsetInDP);
	}

	@Override
	public void getItemOffsets(@NonNull Rect outRect,
	                           @NonNull View view,
	                           @NonNull RecyclerView parent,
	                           @NonNull RecyclerView.State state
	) {

		outRect.left = outRect.right = outRect.bottom = space;
		// Add top margin only for the first item to avoid double space between items
		if (parent.getChildLayoutPosition(view) == 0) {
			outRect.top = space;
		} else {
			outRect.top = 0;
		}
	}
}
