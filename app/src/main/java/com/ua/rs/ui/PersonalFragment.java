package com.ua.rs.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ua.rs.databinding.FragmentPersonalBinding;

import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class PersonalFragment extends Fragment {

	private int    RC_SIGN_IN;
	private int    REQ_PERM_CODE = 1;
	private String TAG           = "TELNUM";

	private FragmentPersonalBinding binding;

	private final List<AuthUI.IdpConfig> providers = Arrays.asList(
			new AuthUI.IdpConfig.PhoneBuilder().build(),
			new AuthUI.IdpConfig.GoogleBuilder().build()
	);


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

//		TelephonyManager tMgr =
//				(TelephonyManager) requireContext().getSystemService(TELEPHONY_SERVICE);
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//			if (requireActivity().checkSelfPermission(READ_SMS) != PERMISSION_GRANTED &&
//			requireActivity()
//					.checkSelfPermission(READ_PHONE_NUMBERS) != PERMISSION_GRANTED && requireActivity()
//					.checkSelfPermission(READ_PHONE_STATE) != PERMISSION_GRANTED) {
//
//				String[] permissions = {
//						READ_SMS, READ_PHONE_NUMBERS, READ_PHONE_STATE
//				};
//				int[] result = new int[3];
//				requireActivity().requestPermissions(permissions, REQ_PERM_CODE);
//				requireActivity().onRequestPermissionsResult(REQ_PERM_CODE, permissions, result);
//			}
//		}
//		String mPhoneNumber = tMgr.getImei(0);
//		Log.d(TAG, "onCreate: " + mPhoneNumber);
		final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

		FirebaseUser user = firebaseAuth.getCurrentUser();
		if (user == null) firebaseAuth.signInAnonymously();
//				.addOnSuccessListener(auth -> PhoneAuthProvider
//						.getInstance()
//						.verifyPhoneNumber(mPhoneNumber, 13, SECONDS,
//								requireActivity(),
//								new OnVerificationStateChangedCallbacks() {
//									@Override
//									public void onVerificationCompleted(
//											PhoneAuthCredential phoneAuthCredential
//									) {
//										firebaseAuth
//												.getCurrentUser()
//												.linkWithCredential(phoneAuthCredential);
//									}
//
//									@Override
//									public void onVerificationFailed(
//											FirebaseException e
//									) {
//										Log.e(TAG, "onVerificationFailed: ", e);
//									}
//								}));
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RC_SIGN_IN) {
			IdpResponse response = IdpResponse.fromResultIntent(data);
			if (resultCode == RESULT_OK)
				if (FirebaseAuth.getInstance().getCurrentUser() != null) changeUserText();
				else {
					try {
						Log.d("FireStore", response.getError().getLocalizedMessage());
						Log.d("FireStore", String.valueOf(response.getError().getErrorCode()));
					} catch (NullPointerException ex) {
						Log.d("FireStore", "error happens!!!");
					}
				}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
	                         ViewGroup container,
	                         Bundle savedInstanceState
	) {
		binding = FragmentPersonalBinding.inflate(inflater, container, false);
		binding.buttonSignOut.setOnClickListener(view -> {
			if (FirebaseAuth.getInstance().getCurrentUser() != null) FirebaseAuth.getInstance().signOut();
			changeUserText();
		});

		binding.buttonSignIn.setOnClickListener(view ->
				startActivityForResult(
						AuthUI.getInstance()
						      .createSignInIntentBuilder()
						      .setAvailableProviders(providers)
						      .build(),
						RC_SIGN_IN)
		);
		changeUserText();

		return binding.getRoot();
	}

	private void changeUserText() {
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		if (user != null)
			binding.TextViewUserName.setText(user.isAnonymous() ? "Anonymous" : user.getPhoneNumber());
	}

	@Override
	public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater
	) {
		super.onCreateOptionsMenu(menu, inflater);
	}
}
