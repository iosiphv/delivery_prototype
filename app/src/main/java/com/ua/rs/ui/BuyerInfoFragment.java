package com.ua.rs.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.ua.rs.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class BuyerInfoFragment extends Fragment {


	public BuyerInfoFragment() {
		// Required empty public constructor
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState
	) {

		return inflater.inflate(R.layout.fragment_buyer_info, container, false);
	}

}
