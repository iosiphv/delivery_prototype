package com.ua.rs.ui;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.ua.rs.ui.DeliveryPointFragmentDirections;
import com.ua.rs.databinding.FragmentDeliveryPointBinding;


/**
 * A simple {@link Fragment} subclass.
 */
public class DeliveryPointFragment extends Fragment {
	private FragmentDeliveryPointBinding bind;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState
	) {
		bind = FragmentDeliveryPointBinding.inflate(inflater, container, false);
		return bind.getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		bind.buttonNext.setOnClickListener(Navigation.createNavigateOnClickListener(DeliveryPointFragmentDirections
				.actionDeliveryPointFragmentToBuyerInfoFragment()));
	}

}
