package com.ua.rs.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProvider.Factory;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ua.rs.R;
import com.ua.rs.adapters.AdapterCartItems;
import com.ua.rs.databinding.FragmentBasketBinding;
import com.ua.rs.pojo.OrderItem;
import com.ua.rs.vm.CartViewModel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static androidx.navigation.Navigation.createNavigateOnClickListener;
import static androidx.recyclerview.widget.DividerItemDecoration.VERTICAL;
import static com.ua.rs.ui.BasketFragmentDirections.actionFragmentBacketToDeliveryPointFragment;
import static java.lang.String.format;

public class BasketFragment extends Fragment {
	private CartViewModel         cartViewModel;
	private FragmentBasketBinding binding;
	private AdapterCartItems      adapter = new AdapterCartItems();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		NavController       controller = NavHostFragment.findNavController(this);
		ViewModelStoreOwner owner      = controller.getViewModelStoreOwner(R.id.nav_graph);
		Factory             factory    = getDefaultViewModelProviderFactory();

		ViewModelProvider viewModelProvider = new ViewModelProvider(owner, factory);

		cartViewModel = viewModelProvider.get(CartViewModel.class);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
	                         ViewGroup container,
	                         Bundle savedInstanceState
	) {
		binding = FragmentBasketBinding.inflate(inflater, container, false);
		return binding.getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		Context context = binding.getRoot().getContext();

		LinearLayoutManager layout = new LinearLayoutManager(context);
		layout.setItemPrefetchEnabled(true);
		layout.setInitialPrefetchItemCount(10);

		adapter = new AdapterCartItems();

		RecyclerView rcItems = binding.itemsToBuy;
		rcItems.setLayoutManager(layout);
		rcItems.setItemAnimator(new DefaultItemAnimator());
		rcItems.addItemDecoration(new DividerItemDecoration(context, VERTICAL));
		rcItems.setHasFixedSize(true);
		rcItems.setDrawingCacheEnabled(true);
		rcItems.setAdapter(adapter);

		binding.buttonNext.setOnClickListener(createNavigateOnClickListener(actionFragmentBacketToDeliveryPointFragment()));

		binding.buttonClear.setOnClickListener(view -> cartViewModel.clearCart());

		cartViewModel.getData().observe(getViewLifecycleOwner(), items -> {
			adapter.setItems(items);

			double sum = items.parallelStream()
			                  .mapToDouble(OrderItem::getOrderedSum)
			                  .sum();
			binding.textCost.setText(format(Locale.getDefault(), "$ %.2f", sum));
		});
	}
}
