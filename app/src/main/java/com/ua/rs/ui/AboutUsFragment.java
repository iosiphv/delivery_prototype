package com.ua.rs.ui;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.ua.rs.R;

import static android.graphics.Typeface.BOLD;
import static android.text.Spanned.SPAN_COMPOSING;
import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;


public class AboutUsFragment extends Fragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	                         Bundle savedInstanceState
	) {
		View view = inflater.inflate(R.layout.fragment_about_us, container,
				false);
		TextView text   = view.findViewById(R.id.about_us_name);
		String   source = getString(R.string.app_name);

		SpannableString str = new SpannableString(source);

		int yellow = ContextCompat.getColor(view.getContext(), R.color.colorPrimaryLight);
		int red    = ContextCompat.getColor(view.getContext(), R.color.colorPrimaryDark);

		str.setSpan(new ForegroundColorSpan(yellow), 0, 4, SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(new ForegroundColorSpan(red), 4, 12, SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(new StyleSpan(BOLD), 0, 12, SPAN_COMPOSING);

		text.setText(str, TextView.BufferType.SPANNABLE);

		return view;
	}
}
