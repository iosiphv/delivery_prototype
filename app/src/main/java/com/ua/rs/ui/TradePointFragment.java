package com.ua.rs.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProvider.Factory;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ua.rs.R;
import com.ua.rs.adapters.AdapterItems;
import com.ua.rs.databinding.FragmentTradePointBinding;
import com.ua.rs.pojo.Item;
import com.ua.rs.vm.CartViewModel;
import com.ua.rs.vm.TradePointViewModel;

import java.util.ArrayList;

import static androidx.navigation.fragment.NavHostFragment.findNavController;
import static androidx.recyclerview.widget.RecyclerView.VERTICAL;

public class TradePointFragment extends Fragment {
	private static final String TAG = "TradePointFragment";

	private TradePointViewModel tradePointViewModel;
	private CartViewModel       cartViewModel;

	private FragmentTradePointBinding binding;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			NavController navController = findNavController(this);
			ViewModelStoreOwner modelStoreOwner =
					navController.getViewModelStoreOwner(R.id.nav_graph);
			Factory           factory  = getDefaultViewModelProviderFactory();
			ViewModelProvider provider = new ViewModelProvider(modelStoreOwner, factory);

			tradePointViewModel = provider.get(TradePointViewModel.class);
			cartViewModel = provider.get(CartViewModel.class);
		} catch (NullPointerException | IllegalStateException ex) {
			Log.e("TradePointFrag", ex.toString());
		}
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
	                         @Nullable Bundle savedInstanceState
	) {
		Log.d(TAG, "onCreateView: ");
		binding = FragmentTradePointBinding.inflate(inflater, container, false);
		Context context = binding.getRoot().getContext();

		AdapterItems adapter = new AdapterItems();
		adapter.setOnItemClickListener(this::onAddToCartClick);

		LinearLayoutManager layout = new LinearLayoutManager(context, VERTICAL, false);
		layout.setItemPrefetchEnabled(true);
		layout.setInitialPrefetchItemCount(10);

		RecyclerView rcItems = binding.items;
		rcItems.setLayoutManager(layout);
		rcItems.setItemAnimator(new DefaultItemAnimator());
		rcItems.setHasFixedSize(true);
		rcItems.setDrawingCacheEnabled(true);
		rcItems.setAdapter(adapter);
		adapter.setData(new ArrayList<>(1));

		tradePointViewModel.getData().observe(getViewLifecycleOwner(), this::updateAndStopRefresh);
		binding.refreshLayout.setOnRefreshListener(() -> tradePointViewModel.getDataFromNet());
		return binding.getRoot();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated: ");
		super.onActivityCreated(savedInstanceState);

		if (binding.items.getAdapter().getItemCount() == 0) {
			binding.refreshLayout.setRefreshing(true);
			tradePointViewModel.getDataFromNet();
		}
	}

	private void onAddToCartClick(Item item, View view) {
		cartViewModel.addToCart(item);
	}

	private void updateAndStopRefresh(ArrayList<Item> items) {
		((AdapterItems) binding.items.getAdapter()).setData(items);
		binding.refreshLayout.setRefreshing(false);
	}

	public interface OnClickListener {
		void onItemClick(Item item, View view);
	}
}
