package com.ua.rs.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ua.rs.databinding.RcBuyedItemBinding;
import com.ua.rs.pojo.OrderItem;

import java.util.ArrayList;

import static androidx.recyclerview.widget.DiffUtil.calculateDiff;

public class AdapterCartItems extends RecyclerView.Adapter<AdapterCartItems.BuyedItemViewHolder> {
	private ArrayList<OrderItem> items = new ArrayList<>(10);

	private String TAG = "AdapterCartItems";

	public void setItems(ArrayList<OrderItem> list) {
		DiffUtil.DiffResult diffResult = calculateDiff(new OrderItemDiffCallback(list, this.items));
		this.items = list;
		diffResult.dispatchUpdatesTo(this);
	}

	@NonNull
	@Override
	public BuyedItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater     from    = LayoutInflater.from(parent.getContext());
		RcBuyedItemBinding binding = RcBuyedItemBinding.inflate(from, parent, false);
		return new BuyedItemViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(@NonNull BuyedItemViewHolder holder, int position) {
		holder.bind(items.get(position));
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	public class BuyedItemViewHolder extends RecyclerView.ViewHolder {
		private RcBuyedItemBinding bind;

		public BuyedItemViewHolder(@NonNull RcBuyedItemBinding binding) {
			super(binding.getRoot());
			bind = binding;

			bind.buttonMinus.setOnClickListener(this::onClickMinus);
			bind.buttonPlus.setOnClickListener(this::onClickPlus);
		}

		private void onClickPlus(View view) {
			bind.getItem().addOne();
			bind.executePendingBindings();
		}

		private void onClickMinus(View view) {
			bind.getItem().removeOne();
			bind.executePendingBindings();
		}

		public void bind(OrderItem item) {
			bind.setItem(item);
			bind.executePendingBindings();
		}
	}
}
