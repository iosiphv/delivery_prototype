package com.ua.rs.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ua.rs.ui.TradePointFragment;
import com.ua.rs.databinding.RcItemsBinding;
import com.ua.rs.databinding.RcItemsSquareBinding;
import com.ua.rs.pojo.Item;

import java.util.ArrayList;

import static androidx.recyclerview.widget.DiffUtil.calculateDiff;

public class AdapterItems extends RecyclerView.Adapter<AdapterItems.ViewHolderItem> {
	private String TAG = "ADAPTER";

	private ArrayList<Item> data = new ArrayList<>(1);

	private TradePointFragment.OnClickListener onItemClickListener;

	public void setData(ArrayList<Item> data) {
		DiffUtil.DiffResult diffResult = calculateDiff(new ItemDiffCallback(data, this.data));
		this.data = data;
		diffResult.dispatchUpdatesTo(this);
	}

	@NonNull
	@Override
	public ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType
	) {
		LayoutInflater  inflater = LayoutInflater.from(parent.getContext());
		ViewDataBinding bind;
		switch (viewType) {
			case 1:
				bind = RcItemsBinding.inflate(inflater, parent, false);
				break;
			default:
				bind = RcItemsSquareBinding.inflate(inflater, parent, false);
				break;
		}

		Log.d(TAG, "onCreateViewHolder: " + viewType);

		return new ViewHolderItem(bind.getRoot());
	}

	@Override
	public int getItemViewType(int position) {
//		return (position / 2) == 0 ? 1 : 2;
		return 1;
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolderItem holder, int position) {
		holder.bind(data.get(position));
	}

	@Override
	public int getItemCount() {
		return data.size();
	}

	public void setOnItemClickListener(TradePointFragment.OnClickListener onItemClickListener) {
		this.onItemClickListener = onItemClickListener;
	}


	class ViewHolderItem extends RecyclerView.ViewHolder {
		private ViewDataBinding bind;

		ViewHolderItem(@NonNull View view) {
			super(view);
			bind = DataBindingUtil.getBinding(view);
		}

		void bind(@NonNull Item item) {
			bind.setVariable(com.ua.rs.BR.item, item);
			bind.setVariable(com.ua.rs.BR.listener, onItemClickListener);
			bind.executePendingBindings();
		}
	}
}
