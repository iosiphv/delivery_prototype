package com.ua.rs.adapters;

import androidx.recyclerview.widget.DiffUtil;

import com.ua.rs.pojo.Item;

import java.util.ArrayList;

public class ItemDiffCallback extends DiffUtil.Callback {
	private ArrayList<Item> newItems;
	private ArrayList<Item> oldItems;

	public ItemDiffCallback(ArrayList<Item> newItems,
	                        ArrayList<Item> oldItems
	) {
		this.newItems = newItems;
		this.oldItems = oldItems;
	}

	@Override
	public int getOldListSize() {
		return oldItems.size();
	}

	@Override
	public int getNewListSize() {
		return newItems.size();
	}

	@Override
	public boolean areItemsTheSame(
			int oldItemPosition,
			int newItemPosition
	) {
		Item _old = oldItems.get(oldItemPosition);
		Item _new = newItems.get(newItemPosition);
		return _old.hashCode() == _new.hashCode();
	}

	@Override
	public boolean areContentsTheSame(
			int oldItemPosition,
			int newItemPosition
	) {
		Item _old = oldItems.get(oldItemPosition);
		Item _new = newItems.get(newItemPosition);
		return _old.equals(_new);
	}
}
