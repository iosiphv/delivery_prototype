package com.ua.rs.adapters;

import androidx.recyclerview.widget.DiffUtil;

import com.ua.rs.pojo.OrderItem;

import java.util.ArrayList;

class OrderItemDiffCallback extends DiffUtil.Callback {
	private final ArrayList<OrderItem> aNew;
	private final ArrayList<OrderItem> aOld;

	public OrderItemDiffCallback(ArrayList<OrderItem> _new, ArrayList<OrderItem> _old) {
		aNew = _new;
		aOld = _old;
	}

	@Override
	public int getOldListSize() {
		return aOld.size();
	}

	@Override
	public int getNewListSize() {
		return aNew.size();
	}

	@Override
	public boolean areItemsTheSame(
			int oldItemPosition,
			int newItemPosition
	) {
		OrderItem _old = aOld.get(oldItemPosition);
		OrderItem _new = aNew.get(newItemPosition);
		return _old.hashCode() == _new.hashCode();
	}

	@Override
	public boolean areContentsTheSame(
			int oldItemPosition,
			int newItemPosition
	) {
		OrderItem _old = aOld.get(oldItemPosition);
		OrderItem _new = aNew.get(newItemPosition);
		return _old.equals(_new);
	}

}
