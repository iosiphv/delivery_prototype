package com.ua.rs.img;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.AppGlideModule;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.StorageReference;

import java.io.InputStream;

@GlideModule
public class GlideImg extends AppGlideModule {
	@Override
	public void registerComponents(@NonNull Context context,
	                               @NonNull Glide glide,
	                               @NonNull Registry registry
	) {
		super.registerComponents(context, glide, registry);
		registry.append(StorageReference.class,
				InputStream.class,
				new FirebaseImageLoader.Factory());

	}

	@Override
	public void applyOptions(@NonNull Context context, @NonNull GlideBuilder builder) {
		int memCacheSize  = 1024 * 1024 * 10;//10 MB
		int diskCacheSize = 1024 * 1024 * 25;//25 MB
		builder.setMemoryCache(new LruResourceCache(memCacheSize));
		builder.setDiskCache(new InternalCacheDiskCacheFactory(context, "RSGlideCache",
				diskCacheSize));
		builder.setLogLevel(Log.DEBUG);
	}
}
