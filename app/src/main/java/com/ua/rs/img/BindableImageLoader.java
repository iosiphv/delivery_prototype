package com.ua.rs.img;

import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import static com.bumptech.glide.load.resource.bitmap.BitmapTransitionOptions.withCrossFade;

public class BindableImageLoader {
	private static String TAG = "Glide";

	@AddTrace(name = "bind_loadImageBuyed")
	@BindingAdapter("loadImageBuyed")
	public static void loadImageBuyed(ImageView v, String img) {
		String msgDigest = "buyed";
		loadData(v, img, msgDigest);
	}

	@AddTrace(name = "bind_loadImageSquare")
	@BindingAdapter("loadImageSquare")
	public static void loadImageSquare(ImageView v, String img) {
		String msgDigest = "square";
		loadData(v, img, msgDigest);
	}

	@AddTrace(name = "loadImageData")
	private static void loadData(ImageView v, String img, String digest) {
		GlideRequest<Bitmap> asBitmap;
		asBitmap = GlideApp.with(v.getContext())
		                   .asBitmap()
		                   .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
		                   .centerCrop()
		                   .override(v.getMeasuredWidth(), v.getMeasuredHeight())
		                   .timeout(100)
		                   .transition(withCrossFade());

		String location = "images/" + img;

		StorageReference ref = FirebaseStorage.getInstance().getReference(location);

		ref.getBytes(Long.MAX_VALUE)
		   .addOnCompleteListener(task -> {
			   if (task.isSuccessful()) {
				   byte[] result = task.getResult();

				   asBitmap
						   .signature(messageDigest -> {
							   String dummyDig = location + digest;
							   byte[] bytes    = dummyDig.getBytes();
							   messageDigest.digest(bytes);
						   })
						   .load(result)
						   .into(v);
			   }
		   });
	}
}
