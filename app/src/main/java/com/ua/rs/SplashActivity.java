package com.ua.rs;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.motion.widget.TransitionAdapter;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.ua.rs.databinding.ActivitySplashBinding;
import com.ua.rs.repository.FireStoreHelper;

import static android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
import static androidx.core.app.ActivityOptionsCompat.makeSceneTransitionAnimation;

public class SplashActivity extends AppCompatActivity {

	private static final String TAG = "SPLASH";

	private ActivitySplashBinding bind;
	private TransitionAdapter     listener;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		bind = DataBindingUtil.setContentView(this, R.layout.activity_splash);

		SpannableString str = new SpannableString(getResources().getString(R.string.app_name));

		int yellow = ContextCompat.getColor(this, R.color.colorPrimaryLight);
		int red    = ContextCompat.getColor(this, R.color.colorPrimaryDark);

		str.setSpan(new ForegroundColorSpan(yellow), 0, 4, SPAN_EXCLUSIVE_EXCLUSIVE);
		str.setSpan(new ForegroundColorSpan(red), 4, 12, SPAN_EXCLUSIVE_EXCLUSIVE);
		bind.tvAppName.setText(str);

		listener = new TransitionAdapter() {
			@Override
			public void onTransitionStarted(MotionLayout motionLayout, int startId, int endId) {
				super.onTransitionStarted(motionLayout, startId, endId);
				if (R.id.start == startId) FireStoreHelper.getFirestoreInstance();
			}

			@Override
			public void onTransitionCompleted(MotionLayout motionLayout, int currentId) {
				super.onTransitionCompleted(motionLayout, currentId);
				if (R.id.end == currentId) {
					Intent intent = new Intent(SplashActivity.this, MainActivity.class);

					ActivityOptionsCompat optionsCompat = makeSceneTransitionAnimation(SplashActivity.this);
					startActivity(intent, optionsCompat.toBundle());
				}
			}
		};
	}

	@Override
	protected void onStart() {
		super.onStart();
		bind.constraintLayout.setTransitionListener(listener);
	}

	@Override
	protected void onPause() {
		super.onPause();
		new Handler().postDelayed(this::finishAfterTransition, 250);
	}
}
