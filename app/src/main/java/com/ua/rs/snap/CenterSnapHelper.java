package com.ua.rs.snap;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.LayoutManager;

public class CenterSnapHelper extends LinearSnapHelper {
	public CenterSnapHelper() {
		super();
	}

	@Override
	public int[] calculateDistanceToFinalSnap(@NonNull LayoutManager layoutManager,
	                                          @NonNull View targetView
	) {
		return super.calculateDistanceToFinalSnap(layoutManager, targetView);
	}

	@Override
	public View findSnapView(LayoutManager layoutManager) {
		return super.findSnapView(layoutManager);
	}

	@Override
	public void attachToRecyclerView(@Nullable RecyclerView recyclerView) throws
	                                                                      IllegalStateException {
		super.attachToRecyclerView(recyclerView);
	}
}
